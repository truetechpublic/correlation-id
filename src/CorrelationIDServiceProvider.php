<?php

namespace TrueTech\CorrelationID;

use Illuminate\Http\Client\Factory as HttpFactory;
use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Queue\Queue;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;
use Sentry\State\Scope;
use Laravel\Lumen\Application as Lumen;
class CorrelationIDServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Register correlation middleware
        if ($this->app instanceof Lumen) {
            $this->app->middleware(CorrelationIDMiddleware::class);
        }

        // Add tracing middleware for lumen HTTP client // TODO not work with many call http client
        /*$this->app->bind(
                HttpFactory::class,
                static function ($app) {
                    return (new HttpFactory())->withMiddleware(\Sentry\Tracing\GuzzleTracingMiddleware::trace());
                }
        );*/

        // Forward correlation id to queue
        Queue::createPayloadUsing(static function (?string $connection, ?string $queue, ?array $payload): ?array {
            $payload['correlation_id'] = correlationId();
            return $payload;
        });

        // Get from queue correlation id
        Event::listen(JobProcessing::class, function(JobProcessing $event) {
            $correlationId = $event->job->payload()['correlation_id'] ?? null;
            correlationId($correlationId);

            if (app()->bound('sentry')) {
                \Sentry\configureScope(function (Scope $scope): void {
                    $scope->setTag('correlation_id', correlationId());
                });
            }
        });

    }
}