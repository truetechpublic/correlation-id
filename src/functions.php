<?php

namespace TrueTech\CorrelationID;


/**
 * Generate or create correlation ID.
 *
 * @param  string|null  $id
 * @return string
 */
if (!function_exists('correlationId')) {
    function correlationId(?string $id = null): string
    {
        if (!is_null($id)) {
            return CorrelationID::set($id);
        }

        return CorrelationID::get();
    }
}