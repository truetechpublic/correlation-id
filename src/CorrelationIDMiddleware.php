<?php

namespace TrueTech\CorrelationID;

use Closure;
use Illuminate\Http\Request;
use Sentry\State\Scope;

class CorrelationIDMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return void
     */
    public function handle(Request $request, Closure $next)
    {

        correlationId($request->header(CorrelationID::CORRELATION_HEADER_KEY));

        if (app()->bound('sentry')) {
            \Sentry\configureScope(function (Scope $scope) use ($request): void {
                $scope->setTag('correlation_id', correlationId());
            });
        }

        return $next($request);
    }
}
