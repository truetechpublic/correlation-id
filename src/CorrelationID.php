<?php

namespace TrueTech\CorrelationID;

use Exception;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

final class CorrelationID
{
    public const CORRELATION_HEADER_KEY = 'x-correlation-id';

    /**
     * @var string The correlation ID.
     */
    protected static $uuid;

    /**
     * Constructor.
     *
     * @codeCoverageIgnore
     */
    private function __construct()
    {
    }

    /**
     * Turns this object into an UUID string.
     *
     * @return string
     */
    public static function get(): string
    {
        if (empty(static::$uuid)) {
            static::$uuid = static::generate()->toString();
        }

        return static::$uuid;
    }

    /**
     * Generates a new correlation ID.
     *
     * @return UuidInterface
     */
    protected static function generate(): UuidInterface
    {
        return Uuid::uuid4();
    }

    public static function set(?string $id): ?string
    {
        if (empty(static::$uuid)) {
            static::$uuid = $id;
        }

        return static::$uuid;
    }
}
